![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Social Media II": Vom konzeptionellen ERM zum logischen ERD

In dieser Übung wandeln sie ein konzeptionellen ERM in das logische ERD um. 

#### Aufgabe

Schauen sie sich das folgende konzeptionelle ERM an. Wandeln sie es in ein logisches ERD um.

![konzERD](x_gitressourcen/SocialMedia_II.png)

Zusätzlich sollen die folgenden Kriterien erfüllt werden:

- Das Datum des Likes soll zusätzlich erfasst werden können, so dass man später danach nach dem Alter sortieren und filtern kann.
- Das Startjahr der Klasse soll auch abgebildet werden (erfasst werden können)
- Ein Post besteht aus einem Text, sowie aus einem *optionalen* Bild oder einem *optionalen* Video

(*optional bedeutet: kann haben oder auch nicht*)

#### Tasks:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- Folgen Sie der Anleitung zur Auflösung von 1/C zu M(C) Beziehungen.
- Folgen Sie der Anleitung zur Auflösung von M(C) zu M(C) Beziehungen.
- Fügen sie weitere Attribute hinzu
- Lösen sie die genannten Kriterien

