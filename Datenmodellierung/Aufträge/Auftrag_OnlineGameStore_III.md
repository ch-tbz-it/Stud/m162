![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Online Game-Store III": Vom konzeptionellen/logischen zum physischen ERD

In dieser Übung wandeln sie ein konzeptionelles/logisches ERD in ein physisches ERD um.

#### Aufgabe

Das Tool MySQL Workbench kann netzwerkförmige Beziehungen automatisch auflösen. Dies haben Sie im entsprechenden Auftrag bereits selbst getestet. Andere Tools können dies nicht.

Dies bringt uns in die komfortable Lage, dass wir nun direkt mit dem konzeptionellen Diagramm arbeiten können. Verwenden sie also nun das konzeptionelle Diagramme aus dem Auftrag "Online Game-Store II" oder ihre eigene Lösung. Wandeln sie es in ein physisches Modell um.

**Tasks**:

- Erstellen sie das ERD mit MySql Workbench
- Erstellen sie alle Beziehungen
- Erstellen sie sinnvolle zusätzliche Attribute für die Tabellen
- Stellen sie sicher, dass alle Attribute, die korrekten Namen und Eigenschaften (FK, NN, UQ, FK) besitzen.

