![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Crypto I": Redundanzen auflösen / Normalisieren

In dieser Übung lösen sie Redundanzen auf und normalisieren die Daten (3. Normalform).

#### Aufgabe

Schauen sie sich die folgenden Daten an und bringen sie sie in die 3. Normalform.

![auftrag](x_gitressourcen/Crypto_I.png)

Tasks:

- Erstellen sie erste Normalform in Excel. Als Grundlage können sie [diese Datei](Auftrag_Crypto_I.xlsx) verwenden.
- Erstellen sie die 2. Normalform in Draw.io
- Erstellen sie die 3. Normalform in MySql Workbench

#### Zeit und Form

- 45 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
