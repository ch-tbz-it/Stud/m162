![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Veloreparatur": Normalisierung

In dieser Übung üben sie die Normalisierungschritte und arbeiten mit MySql Workbench.

Sie betreiben einen Veloreparaturdienst als Start-Up. Bisher hatten sie ihre Kunden in einem Excel gepflegt. Nun wird ihre Kundenbasis aber immer grösser und sie sehen die Probleme die sie bekommen mit dem editieren des Excels (Anomalien). Sie möchten umstellen auf eine richtige Datenbank...

#### Aufgabe

Normalisieren sie die folgende Tabelle und gehen sie dabei Schritteweise vor. **[Die Tabelle finden sie als Excel Auftrag_Veloreparatur.xlsx](Auftrag_Veloreparatur.xlsx)**.

![Bild](x_gitressourcen/Veloreparatur.png)

**Tasks**:

[Normalisierung](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Normalisierung.md)

- Excel: erstellen Sie die **1. Normalform** in Excel
  - kopieren Sie die Zeilen in denen es Mehrfacheinträge hatten
  - füllen Sie fehlende Daten aus

- Papier: erstellen sie die **2. Normalform** auf Papier mit Bleistift (oder in draw.io).
  - erstellen Sie ein konzeptionelles ERM mit den erkannten Entitäten aus der 1. Normalform
  - erstellen Sie ein logisches ERD (mit PKs und mit FKs und anderen wichtigen Attibuten) 
  - überprüfen sie in Excel, ob ihr logisches Schema standhält und erstellen sie die neue Struktur mit Daten

- MySQL-Workbench: erstellen Sie das logisch-physische Design
  - definieren Sie die Entitäten, Relationen und Kardinalitäten
  - erstellen Sie, wenn nötig, die **3. Normalform** aufgrund der 2. Normalform
    *(die dritte Normalform wird in vielen Praxisprojekten nicht modelliert)*
  - überführen Sie die Kolonnen-Namen als Attribute in die MySQL-Workbench

#### Zeit und Form

- 45 Minuten
- Individuell

---

&copy;TBZ, 2021/2024, Modul: m162