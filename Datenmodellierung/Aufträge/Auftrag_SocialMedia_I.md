![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: ERD zu "Social Media" definieren

In dieser Übung geht es darum, dass sie aufgrund einer Beschreibung mit Kriterien ein ERD mit Kardinalitäten erstellen können. 

#### Aufgabe

Erstellen sie ein ERD zum Thema "Social Media", welches die folgenden Kriterien erfüllt.

Wir möchten eine neue **Social Media Applikation** erstellen, die **nur** von allen Lehrpersonen und von allen Lernenden der Abteilung IT der TBZ verwendet werden können. 

- Es gibt keine öffentlichen Einträge. 
- Jede:r Nutzer:in (Lehrperson oder Lernende) kann einen "Text-Post" erstellen. Diesen "Post" kann man liken, aber nicht kommentieren oder darauf antworten. 
- Posts von Lernenden können nur von den Lernenden der gleichen Klasse gelesen werden
- Posts von Lernenden können von 'allen' Lehrpersonen gelesen werden. 
- Eine Lehrperson kann entweder einen Post für eine seiner Klassen erstellen oder auch einen Post für andere Lehrpersonen erstellen.

#### Tasks:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- Fügen Sie die Kardinalitäten hinzu
- Dokumentieren sie ihr ERD und erklären sie wie sie die Kriterien erfüllen können und was ihre Beziehungen bedeuten.
  

