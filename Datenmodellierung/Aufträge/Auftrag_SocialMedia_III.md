![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Social Media III": Vom konzeptionellen/logischen zum physischen ERD

In dieser Übung wandeln sie ein konzeptionelles/logisches in ein physisches ERD um.

#### Aufgabe

Verwenden sie das konzeptionelle Diagramm aus der Übung "Social Media II" oder ihre eigene Lösung. Wandeln sie es in ein physisches Modell um.

**Tasks**:

- Erstellen sie das ERD mit MySql Workbench
- Erstellen sie alle Beziehungen
- Erstellen sie alle Attribute für die Tabellen
- Stellen sie sicher, dass alle Attribute, die korrekten Namen und Eigenschaften (FK, NN, UQ, FK) besitzen.

