![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Velotour": Von Aufgabenstellung zu Datenbank

In dieser Übung üben sie die Normalisierungschritte und arbeiten mit MySql Workbench.

Ein Touristikunternehmen organisiert begleitete Velotouren, für welche sich Feriengäste anmelden können. Dabei gibt es verschiedene Typen von Touren, welche immer wieder durchgeführt werden. Daneben pflegen sie auch noch einen Veloverleih, bei dem Velos für die angebotenen Touren gemietet werden können. Mietvelos werden aber nur an Tourenteilnehmer abgegeben.
Folgende Informationen müssen verwaltet werden:

- Personalien des Gastes (Name, Vorname, Anrede, Geburtstag, Strasse, PLZ, Ort, Telefon, E-Mail, Partner, Kinder).
- Angebotene Velotouren (Tour-Name, Beschreibung, Schwierigkeitsgrad, Länge, Anforderung, Start- und Zielort, Preis).
- Termine an denen diese Velotouren stattfinden.
- Verwaltung der Mietvelos (Hersteller, Typ, Marke, Grösse, Anschaffungspreis, -datum, Mietpreis, Wartung, Zubehör).
- Erfassen ob ein Gast ein oder mehrere Velos braucht (Datum, Versicherung nötig?, Kaution).

#### Aufgabe

Überlegen Sie sich, ob diese vorhandenen Informationen genügend/ausreichend sind. Was kann oder sogar muss geändert werden, damit die Lösung besser wird. Welche bisher nicht verwendeten Informationen könnten für eine Datenbanklösung in Zukunft sinnvoll sein? Ergänzen sie diese.

**Tasks**:

- Erstellen sie ein konzeptionelles Modell in draw.io (oder von Hand mit Papier und Bleistift)
- Erstellen sie anschliessend direkt das physische Modell in MySql Workbench
  - Sie dürfen gerne den zwischenschritt via logischem Modell gehen.
  - Das physische Modell muss in der 3. Normalform sein.
- Sie dürfen das Modell verbessern, sofern es noch die gleichen Anforderungen abdecken kann.

#### Zeit und Form

- 120 Minuten (Teil 1 ist auch zu zweit zu empfehelen)
- Individuell

---

&copy;TBZ, 2021/2024, Modul: m162