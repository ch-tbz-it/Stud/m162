![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Tramlinien": ERD Aufgrund von Tabellen erstellen

In dieser Übung erstellen wir ein logisches ERM und ein physisches ERD aufgrund von vorhandenen Tabellen und Daten. Sie üben also die Struktur aufgrund von Daten zu erkennen und zu erstellen.

#### Aufgabe

Die Daten beschreiben ein vereinfachtes Tramsystem einer Stadt. 

Schauen sie sich die folgenden Daten an und erstellen sie das logische ERM in **draw.io** (oder von Hand mit Bleistift und Papier) **und anschliessend** auch das physische in **MySql Workbench**. 

Stellen Sie sicher, dass Sie die korrekten Kardinalitäten bestimmen (hinschreiben). Die Daten geben Hinweise darauf. In MySql Workbench sollen Sie dann auch die korrekten Eigenschaften einsetzen (z.B. NN = 'not null').

![daten](x_gitressourcen/Tramlinien.png)

