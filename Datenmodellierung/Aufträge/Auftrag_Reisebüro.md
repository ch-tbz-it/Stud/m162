![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Reisebüro": Redundanzen erkennen

Sie können aus einer gegebenen Situation Daten entsprechend strukturieren. Diese können Sie danach in einem Tool (Access, Workbench) erstellen

#### Aufgabe

In der [dieser Excel-Datei &#128196;](Auftrag_Reisebüro.xlsx) finden Sie eine Tabelle mit Verkaufsdaten eines kleinen Reisebüros. Ihre Aufgabe ist es, diese bisher verwendete Excel-Applikation in einer Datenbank abzubilden. Informationen zur Aufgabenstellung:

- Die Flugnummer bestimmt die Route des Fluges, d. h. die gleiche Route (von ... nach ...) hat stets die gleiche Flugnummer.
- Eine Flugroute wird an mehreren Tagen im Jahr geflogen, aber nie mehrmals am gleichen Tag (dann hätte sie eine weitere Flugnummer).
- Sie sollen in der Datenbank auch die Preise für die Hotelübernachtungen und die Flüge eingeben können.

**Tasks**:

- Erstellen Sie ein physisches ERM.
- Die Aufteilung der Attribute in mehrere Tabellen muss dabei der 3. Normalform entsprechen.

#### Zeit und Form

- 120 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
