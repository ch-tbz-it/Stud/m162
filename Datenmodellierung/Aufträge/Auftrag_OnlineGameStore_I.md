![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: ERD zu "Online Game-Store" definieren

In dieser Übung geht es darum, dass sie sich mit Entitäten und deren Zusammenhängen beschäftigen. Sie werden sehen, dass es verschiedenen Lösungen gibt, abhängig von den Annahmen, die man trifft. Lassen sie ihrer Kreativität freien Lauf.

#### Aufgabe

Erstellen sie ein ERD zum Thema "Online Game-Store". Sie sollen ein ERD zu dem Game-Store "*Steam*" (oder Battle-Net, etc) erstellen. Welche Entitäten und Verbindungen finden sie? Wenn Sie kein Game-Store kennen, verwenden Sie einfach einen Shop (z.B. H&M oder Online-Shops wie Amazon, etc). 

**Tasks**:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- Dokumentieren sie ihr ERD in Prosa, so dass klar wird, welche Annahmen sie getroffen haben.
  

