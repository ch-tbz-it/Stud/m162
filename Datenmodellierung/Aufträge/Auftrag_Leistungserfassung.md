![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Leistungserfassung": ERM/ERD erstellen

Es soll eine Applikation oder eine App entstehen, mit der man seine "geleisteten" (Arbeits-)Stunden erfassen kann. Es geht um die Stunden, die für ein Projekt geleistet werden mit dem Ziel, am Schluss Rechnungen erstellen zu können.

Alle geleisteten (Arbeits-)Stunden haben vordefinierte Tarife die angewendet werden müssen. Der Tarif ist abhängig von der Rolle, die ein Mitarbeiter aktuell bekleidet. Es ist also ein Unterschied, ob Kevin als Projektleiter, als Sachbearbeiter oder als Hilfsperson eingesetzt ist.

Jedes Projekt ist einem Kunden zugewiesen. 

Aufgrund der geleisteten Stunden auf einem Projekt wird dem Kunden monatlich oder für ein anderes Zeitintervall eine Rechnung geschickt.


#### Aufgabe

Überlegen Sie sich und diskutieren Sie das logische und konzeptionelle Modell auf Papier im kleinen Team.

**Tasks**:

- Erstellen sie ein konzeptionelles Modell auf Flip-Chart (auf Papier)
- Definieren Sie die Beziehungen und die Kardinalitäten
- Erfinden Sie für alle Entitäten die entsprechenden Attribute


#### Zeit und Form

- 40 Minuten
- im Team zu 4 Personen

---

&copy;TBZ, 2024, Modul: m162