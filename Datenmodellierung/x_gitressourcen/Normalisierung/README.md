M162 Daten analysieren und modellieren

# Normalisierung

![NF1](NF1.png)

## 1. Normalform

Step 1

![NF1-Step1](NF1-Step1.png)

<br>

Step 2

![NF1-Step2](NF1-Step2.png)

<br>

Step 3

![NF1-Step3](NF1-Step3.png)

## 2. Normalform

Step 1

![NF2-Step1](NF2-Step2.png)

<br>

Step 2

![NF2-Step2](NF2-Step2.png)

<br>

2.1

![NF2_1](NF2_1.png)


2.2

![NF2_2](NF2_2.png)


2.3

![NF2_3](NF2_3.png)

<br>

Step 3

![NF2-Step3](NF2-Step3.png)

<br>

![NF2-Datenmodell-Var1](NF2-Datenmodell-Var1.png)

![NF2-Datenmodell-Var2](NF2-Datenmodell-Var2.png)

<br>

<br>

![NF3](NF3.png)

<br>

![NF3_2](NF3_2.png)