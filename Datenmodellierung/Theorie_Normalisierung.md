# Theorie Normalisierung

[TOC]

## Normalformen

Die Normalformen geben Regel vor wie man Daten so in Entitäten trennen kann, dass keine Redundanzen mehr bestehen. Es existieren 5 Normalformen, üblicherweise werden die ersten 3 angewandt. Auch wir behandeln hier nur die ersten drei.

Folgend nochmals die Daten mit Redundanzen als Basis für die Umwandlung.

![Redundazen](x_gitressourcen/Redundanzen/redundanzen.png)

### Normalform 1

***Die erste Normalform (1NF) ist dann erfüllt, wenn die Wertebereiche der Attribute des Relationstypen atomar vorliegen.***

![Redundazen](./x_gitressourcen/Normalisierung/NF1.png)

Es wurden zwei Routinen angewandt:

- Alle zusammengesetzte Attribute wurden in eigene Attribute getrennt (z.B. Name, Anschrift, Wohnort, Rechnungsnummer- und Datum, Betrag und Währung). Dies führt zu mehr Spalten als in der Ursprungstabelle.
- Alle Attribute, die mehrere Werte des gleichen Typs haben, wurden auf mehrere Zeilen getrennt (z.B. Artikel). Dies führt zu mehr Datensätzen.

### Normalform 2

***Ein Relationstyp (Tabelle) befindet sich genau dann in der zweiten Normalform (2NF), wenn er sich in der ersten Normalform (1NF) befindet und jedes Nichtschlüsselattribut von jedem Schlüsselkandidaten voll funktional abhängig ist.***

**Vollständig funktionale Abhängigkeit**: wenn dass Nicht-Schlüsselattribut nicht nur von einem Teil der Attribute eines zusammengesetzten Schlüsselkandidaten funktional abhängig ist, sondern von allen Teilen eines Relationstyps.

Es gibt verschiedene Arten wie sie vorgehen können (unter Quellen finden sie Links zu Vorgehensweisen), wobei manche darauf aufbauen, dass bereits Schlüssel vorhanden sind. Dies ist aber nicht immer der Fall wie zum Beispiel mit unseren Daten. Ein einfaches Vorgehen ist folgendes:

1. Entitäten identifzieren. Trennen sie mögliche Objekt-Typen voneinander. Im Beispiel hier haben wir *Kunden*, *Rechnungen* und *Artikel*.
2. Definieren sie für jede Entität einen Primärschlüssel. Sie dürfen gerne eine zusätzliche Spalte mit einer Laufnummer einfügen.
3. Kardinalitäten zwischen den Entitäten identifzieren.
4. Netzwerk-Beziehungen auflösen und FKs setzen.

![2NF_1](./x_gitressourcen/Normalisierung/NF2_1.png)

Im folgenden Schema wurden die identifizierten Entitäten nun getrennt. Die Währung wurde weggelassen, da diese anscheinend immer *SFr* ist und darum nicht in der Datenbank gespeichert werden muss. Sobald man mehrere Währungen beim Kauf zulässt, kann man diese wieder einführen. Dafür wurde beim Artikel eine Beschreibung eingefügt, die normalerweise vorhanden ist und damit in der Entität nicht nur eine Id gespeichert ist.

![2NF_1](./x_gitressourcen/Normalisierung/NF2_2.png)

Zu diesem Zeitpunkt haben wir aber die Information verloren, welche Rechnung zu welchem Kunden gehört und welche Artikel gekauft wurden. Mit den Assoziationen haben wir aber gelernt wie wir FKs einfügen. Wir müssen also lediglich die Kardinalitäten zwischen den Tabellen erkennen und entsprechend können wir FKs einfügen.

![2NF](./x_gitressourcen/Normalisierung/NF2-Datenmodell-Var1.png)

Das Resultat sind folgende Tabellen, die genau das Datenmodell abbilden. Gelb markiert sind die hinzugefügten Spalten. Zusätzlich wurde die Spalten *Anzahl* in der Tabelle *RechnungHatArtikel* eingeführt, damit ein Artikel, der mehrmals in einer Rechnung erscheint, nicht mehrere Datensätze benötigt. Dies wäre zwar keine Redundanz, aber trotzdem zu vermeiden.

![2NF_1](./x_gitressourcen/Normalisierung/NF2_3.png)

### Normalform 3

***Ein Relationstyp befindet sich genau dann in der dritten Normalform (3NF), wenn er sich in der zweiten Normalform (2NF) befindet und kein Nichtschlüsselattribut transitiv von einem Kandidatenschlüssel abhängt.***

Dies bedeutet, dass jedes Attribut **nur** vom PK abhängen darf und nicht noch von anderen Attributen. In unserem Beispiel trifft dies aber auf das Attribut *Wohnort* zu. Dieses ist abhängig vom PK (KundenId) und auch von der PLZ.

![3NF_1](./x_gitressourcen/Normalisierung/NF3.png)

Die dritte Normalform löst man also auf, indem man die transitiv abhängigen Attribute in weitere Entitäten verlagert. Es wurde zusätzlich eine Spalte *PLZId* hinzugefügt. Dies haben wir gemacht, weil einige PLZ mehrmals vorkommen für unterschiedliche Orte und sich dieser Werte daher nicht als PK eignet.

![3NF_1](./x_gitressourcen/Normalisierung/NF3_2.png)

Nach der genanten Logik ist ja eigentlich auch die  eigentlich auch die Hausnummer und Strasse eine transitive Abhängigkeit. Wenn man sich dies genauer überlegt, ist es tatsächlich so, dass mehrere Personen an der gleichen Adresse wohnen können und die Daten demnach - im aktuellen Modell - redundant gespeichert werden.

Oft wird die 3. Normalform genau für Adressdaten nicht angewandt, weil weil der Aufwand nicht mehr vernünftig ist. Stellen sie sich aber eine Kartographie Applikation vor. In solch einer Umgebung, wäre die Trennung wieder zwingend, weil die Strassennamen aktuell sein müssen, falls Änderungen geschehen.

## Weitere Quellen

- <https://www.tinohempel.de/info/info/datenbank/normalisierung.htm>: Anleitung zur Normalisierung
- <https://www.datenbanken-verstehen.de/datenmodellierung/normalisierung/>: Weitere Anleitung zur Normalisierung
