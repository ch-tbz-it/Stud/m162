![TBZ Logo](../x_gitressourcen/tbz_logo.png)
# Einfache Diagrammtypen

[TOC]

Im Folgenden werden die wichtigsten Diagrammtypen behandelt. Es gibt viele mehr, die spezielle [Einsatzzwecke](https://www.bfs.admin.ch/bfs/de/home.html) haben. Alle zu behandelt sprengt den Rahmen dieses Moduls. 

## Liniendiagramm

- **Zeigen zeitlichen Verlauf oder Entwicklung (Trend) auf**.

- Meistens für Vergleich über Zeit oder Perioden.

- Viele Datenpunkte und mehrere Serien möglich.

- *ACHTUNG: Die Achsen werden evtl. nicht linear (Datenlücken) gezeichnet --> XY-Achsen-Diagramm verwenden!*
  

![Liniendiagramm](x_gitressources/Liniendiagramm.png)


## Säulendiagramm

- Balken- und Säulendiagramm gleichwertig (vertikal oder horizontale Bereiche)
- **Werden bei Vergleichen von Kategorien verwendet**.
- Wird bei vielen Serien (<= 8) angewendet
- Direktvergleich von mehreren Serien möglich: gruppiert 
- Direktvergleich der Gesamtwerte über Kategorien hinweg: gestapelt
- Direktvergleich der Proportionen über Kategorien hinweg: proportional-gestapelt
- Kategorien können sortiert werden. 
- Achsen kann man skalieren.

![Säulendiagramm](x_gitressources/Saulendiagramm.png)

![SäulenBalkendiagramm](x_gitressources/SaulenBalkendiagramm.png)

## Balkendiagramm

- Wie Säulendiagramm
- Wird bei vielen Serien (>= 8) angewendet
- Wird oft sortiert nach Kategorien (Rangfolge)

![Balkendiagramm](x_gitressources/Balkendiagramm.png)


## Kreisdiagramm

- **Gute Übersicht einer Verteilung, bzw. Proportionen (Vollkreis = 100%)**.
- Nicht mehr als 5 Kategorie
- Farben mit ausreichend Kontrast
- Mehrere Serien müssen mit mehreren Diagrammen abgebildet werden.
- Anstelle von gestapelten, proportionalen Säulen!
- *ACHTUNG: Kuchendiagramm (3D) verzerrt Proportionen!*


![Kuchendiagramm](x_gitressources/kuchendiagramm.png)





