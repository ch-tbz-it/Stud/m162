![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Modulnoten

In dieser Übung werden sie mit Excel arbeiten, ähnlich wie sie es im Unterricht gesehen haben. Als Grundlage dient ein Beispiel mit Noten für Module.

Ob sie die das Semester wiederholen müssen, hängt von folgenden **fiktiven** Kriterien ab:

- Sie dürfen nicht mehr als 1 ungenügende Modulnote haben
- Die Anzahl Minuspunkte darf nicht höher als 1 sein. Ein Minuspunkt ist die Differenz zu der Note 4. Eine Modulnote von einer 3.5 wäre also 0.5 Minuspunkte.
- Der Schnitt aller Modulnoten muss mindestens 4 sein.

#### Aufgaben

- Laden sie die [TXT-Datei](./Auftrag_Modulnoten/Auftrag_Modulnoten_Noten.txt) herunter.  
- Importieren sie die Daten in Excel. Ob sie zuvor in CSV umwandeln ist fakultativ
- Berechnen sie die notwendigen Felder. Das folgende Bild zeigt wie das Ergebnis aussehen soll, wobei die farblich hervorgehobenen Felder berechnet werden müssen.
- Notwendige Formeln: average, mround, countif, sumif, if, and
- [Hilfestellungen für Excel (z.B. Formeln)](../Excel.md)

<img src="Auftrag_Modulnoten/noten-struktur.png" alt="noten-struktur" style="zoom:60%;" />

![Noten-struktur-resultat](./Auftrag_Modulnoten/Noten-struktur-resultat.png)

#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162