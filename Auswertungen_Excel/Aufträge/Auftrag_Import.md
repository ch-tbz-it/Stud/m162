![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Import

#### Aufgabe a)

Importieren Sie die Datei [Mannschaft.txt](./Auftrag_Import/Mannschaft.txt) in Excel. Folgen Sie den Schritten im [Manual](../Excel-txt-import.md). 

#### Aufgabe b)

Importieren Sie die Datei [Fragebogen.txt](./Auftrag_Import/Fragebogen.txt) in Excel. In der Datei finden Sie die Daten eines Fragebogen-Exports aus Google-Forms.

#### Aufgabe c) Knobelaufgabe

Importieren Sie die Datei [Partei.txt](./Auftrag_Import/Partei.txt) in Excel. In der Datei finden sie die Parteizugehörigkeit über die Jahre. Finden sie raus wie Sie diese Datei importieren können? Tipps:

1. Finden Sie heraus was die Symbole bedeuten
2. Sie benötigen wahrscheinlich Notepad++ (oder ähnliches), um die Daten vor dem Import anzupassen.

Diskutieren Sie die Möglichkeiten mit ihrem/r Tischnachbarn/in.