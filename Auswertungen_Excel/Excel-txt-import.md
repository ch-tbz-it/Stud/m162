![TBZ Logo](../x_gitressourcen/tbz_logo.png)

# Excel TXT-Datei Import

> Wichtig: Um den Assistenten zu starten, muss die txt-Datei innerhalb von EXCEL geöffnet werden! <br> (Bei WIndows-11 sehen die Fenster etwas anderst aus!)

Menü **Öffnen** und Txt-Datei suchen: Bei den Dateitypen nach **allen Dateien** filtern oder nach dem spezifischen Datei-Typ, den Sie importieren möchten.

![Schritt1](./x_gitressources/excel_import_txt_1.png)

---

Achten Sie auf die Vorschau. Wenn die **Umlaute** oder andere Sonderzeichen nicht korrekt dargestellt werden, dann ist ihre Datei höchst wahrscheinlich UTF-8 encodiert und Sie müssen das Encoding entsprechend auswählen. Suchen Sie im Dropdown weiter unten (scrollen!) nach UTF-8.

Wenn ihre Daten eine **Spaltenüberschrift** aufweisen, markieren Sie das Header-Feld!

![Schritt2](./x_gitressources/excel_import_txt_2.png)

---

Legen Sie hier fest, welches Zeichen ihr **Separator** (engl. **Delimiter**) ist. Sie können auch ein beliebiges Zeichen angeben. Wichtig ist ebenfalls, dass Sie auswählen, welches Zeichen ihre **Zeichenketten** umgibt. Dies ist entweder das **Double-Quote** (doppeltes Anführungszeichen) oder das **Single-Quote** (einfaches Anführungszeichen).

![Schritt3](./x_gitressources/excel_import_txt_3.png)

