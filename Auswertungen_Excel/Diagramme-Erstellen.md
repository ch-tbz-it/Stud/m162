![TBZ Logo](../x_gitressourcen/tbz_logo.png)
# Diagramme erstellen

[TOC]



## Bezeichnungen

![bezeichnungen](x_gitressources/bezeichnungen.png)



## Vorgehen Diagramm Erstellen

Folgend finden Sie eine Anleitung wie Sie immer zu einem korrekten Diagramm kommen. Es gibt auch Abkürzungen, z.B. wenn die Kategorien und die Serien gleich nebeneinanderliegen wie im vorliegenden Beispiel. Mit der hier beschriebenen Methode, können Sie aber immer die richtigen Daten auswählen.

![Diagrammerstellen_1](x_gitressources/Diagrammerstellen_1.png)

1. Markieren Sie das Feld in Excel an dem Sie das Diagramm einfügen möchten
2. Fügen Sie unter dem Menupunkt "Insert" ein Diagramm hinzu ohne, dass Sie vorher Daten markiert haben. Ein leeres Diagramm wird hinzugefügt.



![Diagrammerstellen_2](x_gitressources/Diagrammerstellen_2.png)

3. Machen Sie einen Rechtsklick auf die leere Diagrammfläche und wählen Sie "Select Data..." aus. Es öffnet sich das folgende Fenster



![Diagrammerstellen_3](x_gitressources/Diagrammerstellen_3.png)

4. Mit \[Add\] fügen Sie die Serien/Datenreihen hinzu: <br> Klicken Sie hier drauf um eine Reihe hinzuzufügen. Es öffnet sich wieder ein neues Fenster (Siehe unten)
5. Auf diesen Punkt kommen wir später zurück



![Diagrammerstellen_4](x_gitressources/Diagrammerstellen_4.png)

6. Sie haben im neuen Fenster zwei Felder. Das erste Feld ist für den Namen der Datenreihe. Klicken Sie in das Feld und anschliessend auf den Bereich des Excels, welches die Bezeichnung der Reihe enthält. 
7. Klicken Sie anschliessend in das zweite Feld und löschen den vor eingefüllten Inhalt. Anschliessend markieren Sie in Excel alle Datenpunkte, die zu dieser Datenreihe/Serie gehören. Sie sehen im obigen Bild bereits das Resultat. Sie können das Fenster bestätigen und so schliessen.



![Diagrammerstellen_5](x_gitressources/Diagrammerstellen_5.png)

5. Nun sind Sie zurück auf dem vorherigen Fenster und Sie sehen, dass Rubriken / Kategorie automatisch hinzugefügt wurden. Es handelt sich hier aber nur um eine Nummerierung. Wir möchten nun die echten Rubriken hinzufügen. Klicken Sie daher auf das Feld.



![Diagrammerstellen_6](x_gitressources/Diagrammerstellen_6.png)

8. Auch hier öffnet sich wieder ein neues Fenster mit einem Feld. Löschen Sie den Inhalt und markieren Sie die Bezeichnungen, die zu der Datenreihe hinzugefügt wird. Bestätigen Sie ihre Auswahl mit "OK" und so kehren Sie zu dem Fenster zurück welches Sie ebenfalls mit "OK" bestätigen. Sie haben nun die wichtigsten Elemente hinzugefügt. Für eine zusätzliche Serie gehen Sie genau gleich vor. <br>
   **Hinweis**: Jede Serie enthält die gleichen Kategorien. Die Kategorien müssen Sie daher nur einmal definieren



![Diagrammerstellen_7](x_gitressources/Diagrammerstellen_7.png)

9. Wenn Sie das Diagramm in Excel markiert haben, finden Sie unter dem Menupunkt "Chart Design" den Knopf "Add Chart Element". Hier können Sie Titel, Achsenbeschriftungen, Legenden und weiteres hinzufügen und einstellen



## Zusätzliche Hinweise

Excel versucht automatisch die Daten korrekt darzustellen. 

**Beispiel mit Säulendiagramm und einer Serie**: Die Serienbezeichnung (Punkt 6 oben) wird automatisch als Diagrammtitel übernommen. Wenn Sie die Legend einblenden wird dort der gleiche Text stehen.

**Beispiel mit Säulendiagramm und zwei Serien**: Die Serienbezeichnungen (nun in Mehrzahl) können nicht mehr als Diagrammtitel walten. Daher wird der Titel weggelassen und die Bezeichnungen werden in der Legende dargestellt. Beide Elemente (Titel und Legende) sind aber per Standard ausgeblendet.

Natürlich haben die verschiedenen Diagrammtypen noch zusätzliche Einschränkungen, die jeweils zu beachten sind. Ein Kuchendiagramm kann z.B. nur eine Serie haben. Bei mehreren Serien werden pro Serie ein Kuchendiagramm erstellt.

