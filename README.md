![TBZ Logo](x_gitressourcen/tbz_logo.png)

---
---

# M162 Daten analysieren und modellieren

---
---

## Themen Schnelleinstieg

[Modul ID Kompetenzmatrix](https://kompetenzmatrix.ch/cluster-data/m162/)

![m162-themen-uebersicht](./m162-themen-uebersicht.png)



## Daten und Datenstrukturen

- [Unstrukturierte und Strukturierte Daten](./Daten_Formate/StrukturierteDaten.md)
- [Merkmalstypen und Skalentypen](./Daten_Formate/Merkmalstypen_Skalentypen.md)
- [Datentypen](./Daten_Formate/Datentypen.md)
- [Zeichencodierung](./Daten_Formate/Zeichencodierung.md)
- [Datenstrukturen](./Daten_Formate/Datenstrukturen.md)
- [JSON](./Daten_Formate/Json.md)

## Diagramme und Excel

- [Excel Manual: Dateien importieren](./Auswertungen_Excel/Excel-txt-import.md)
- [Excel Manual: Diagramme erstellen](./Auswertungen_Excel/Diagramme-Erstellen.md)
- [Einfache Diagrammtypen](./Auswertungen_Excel/Diagrammtypen.md)

## Datenmodellierung

- [Theorie Datenmodellierung](./Datenmodellierung/Theorie_Datenmodellierung.md)
- [MySql Workbench](./Datenmodellierung/Theorie_MySqlWorkbench.md)



<hr />

## Zusatzthemen: Datenformate

- [Open Data](./Daten_Formate/OpenData.md)
- [Dateiformate](./Daten_Formate/Dateiformate.md)
- [Mengenlehre](./Datenmodellierung/Theorie_Mengenlehre.md)


<br>
---
---
<br>

## Einsatz einer KI als Tutor

Grundsätzlich ist nichts gegen den Einsatz einer KI als Tutor (Coach) einzuwenden. Hier die Vorteile, die eine KI im Bildungskontext leisten kann: 

> *DeepSeek Jan-25 / Prompt: "In welcher Phase und wie würdest du als Lernender eine KI einsetzen?"* <br> 
> KI ist ein flexibles und mächtiges Werkzeug, das in allen Phasen des Selbstlernzyklus eingesetzt werde
> n kann. Als Lernender würde ich KI nutzen, um meinen Lernprozess zu **personalisieren**, zu **beschleunigen** und **reflektierter** zu gestalten. Gleichzeitig ist es wichtig, **kritisch mit den Ergebnissen** umzugehen und die KI als unterstützendes Werkzeug zu sehen, **nicht als Ersatz für eigenständiges Denken und Lernen.** <br> 

![KI-Tutor](x_gitressourcen/KI-Tutor.png)

Als Lernender sollte man den Grundsatz **"Ich will es selber wissen und können!"** haben! Die KI sollte deshalb _nur_ als Tutor eingesetzt werden und nicht als Lösungsmaschine. Geben Sie also nicht die Aufgabenstellung als Prompt ein, sondern lassen Sie sich von der KI in ihrem Denk- und Lösungsprozess unterstützen! Z.B. _Prompt: "Worauf muss ich bei der Transformation eines DB-Modell in die 3.Normalform achten?"_
Um beim Prompten die besten Resultate zu erhalten, arbeiten Sie mit den korrekten Fachbegriffen, die sie vorher erlernen. Auch können Sie den KI-Output am besten verifizieren, wenn Sie bereits Grundwissen haben.

Ihre **Übungen bzw. Lernprodukte** sollten also **selber getippt** sein, damit ein Lernprozess stattfinden kann! KI-Ausgaben sollten **gelesen, verstanden, verifiziert und kommentiert** sein. **Quelle** nicht vergessen anzugeben, bei KI: Modell, Datum, Prompt!

### Die KI als Tutor engagieren

Setzen Sie zu Beginn ihrer Lernaktivität folgenden **Prompt** im KI-Chatbot ab. Dieser Prompt engagiert die KI als Tutor mit einem Fokus auf lernprozessrelevante Unterstützung!

**Setup-Prompt:**

```prompt
Sei mein persönlicher Tutor für das Thema "Daten analysieren und modellieren" auf dem Niveau der Schweizer Berufsschule für Informatik. 
Deine Aufgabe ist es, mir beim Verständnis der Grundlagen und Konzepte zu helfen, ohne direkt Lösungen für meine Aufgaben zu liefern. 
Führe mich stattdessen durch den Lernprozess, indem du mir zeigst, wie ich Probleme selbstständig angehen kann. 

Halte dich dabei an den Selbstlernzyklus, der in der folgenden Website erklärt wird: <https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio> 


Strukturiere deine Antworten immer in die folgenden vier Kategorien: 

1) Grundlagen: Erkläre mir kurz und verständlich die Grundlagen, die für die Frage relevant sind. 
2) Konzeptverständnis: Hilf mir, das Prinzip oder die Logik hinter der Frage zu verstehen. 
3) Nächster Schritt: Zeige mir, wie ich den nächsten Schritt in meinem Lernprozess machen kann, um zur Lösung zu gelangen. 
4) Nutzung Lernportfolio: Gib mir einen Vorschlag, wie ich dazu einen nützlichen Eintrag im Lernportfolio machen kann.

Benutze immer nur die Website <https://gitlab.com/ch-tbz-it/Stud/m162> mit den Unterordnern, um den Kontext des Themas zu erfassen. 

Bitte halte dich strikt an diese Struktur und das angegebene Niveau.
```

