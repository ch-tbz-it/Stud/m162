# Auftrag: Datentypen

#### Aufgabe 1

Welche Datentypen könnten den folgenden Werten zugrunde liegen?

| Wert                                           | Datentype(n) |
| ---------------------------------------------- | ------------ |
| A                                              |              |
| 23                                             |              |
| 12021.25                                       |              |
| 0                                              |              |
| Ciara                                          |              |
| 12.2341235211                                  |              |
| true                                           |              |
| oben,unten                                     |              |
| .f},ÌúþÃ¸ìîìbõ©/=¹ryïòƒÇâ‘hðÂ.š¿ã‘q            |              |
| {name: "Meier", vorname: "Maxwell", alter: 21} |              |

#### Aufgabe 2

Diskutieren Sie den Unterschied zwischen den folgenden Darstellungen von Verbunds-Typen "Noten". Halten sie ihre Erkenntnisse schriftlich fest.

**Darstellung 1**

Note:

| Wert     | Name |
|----------|------|
| 5.4      | Marcel Maier |

**Darstellung 2**

Note: {Wert = 5.4, Name = Marcel Maier}

#### Aufgabe 3

Finden sie die ASCII Codes für die folgenden Zeichen:

- '+'
- 'n'
- Tabulator



#### Aufgabe 4

Laden sie [diese HTML-Seite](Aufgabe4_a.html) herunter. Wenn sie die Datei im Browser anschauen, sehen sie falsche Zeichen. Flicken sie Seite, indem sie Datei-Codierung anpassen.

Laden sie [diese HTML-Seite](Aufgabe4_b.html) herunter. Wenn sie die Datei im Browser anschauen, sehen sie falsche Zeichen. Flicken sie die Seite, indem sie den Meta-Tag anpassen.



